package com.example.shibe.model

import com.example.shibe.model.remote.DogService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideDogService(): DogService = DogService.getInstance()
}