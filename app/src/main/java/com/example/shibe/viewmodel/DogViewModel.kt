package com.example.shibe.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.shibe.model.DogRepo
import com.example.shibe.view.dog.DogState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DogViewModel @Inject constructor(private val dogRepo: DogRepo) : ViewModel() {

    val state: LiveData<DogState> = liveData {
        emit(DogState(isLoading = true))
        val dogs = dogRepo.getDogs(10)
        emit(DogState(dogs = dogs))
    }
}





























