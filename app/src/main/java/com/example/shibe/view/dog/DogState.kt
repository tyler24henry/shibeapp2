package com.example.shibe.view.dog

data class DogState(
    val isLoading: Boolean = false,
    val dogs: List<String> = emptyList()
)
