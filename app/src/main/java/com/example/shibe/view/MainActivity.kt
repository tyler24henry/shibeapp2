package com.example.shibe.view

import androidx.appcompat.app.AppCompatActivity
import com.example.shibe.R
import dagger.hilt.android.AndroidEntryPoint

// inform hilt that this is where the app begins
@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)