package com.example.shibe.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.shibe.databinding.ItemDogBinding

class DogAdapter(
    private val dogs: List<String>,
    private val dogClicked: (dogStr: String) -> Unit
) : RecyclerView.Adapter<DogAdapter.DogViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): DogViewHolder = DogViewHolder.getInstance(parent).apply {
        itemView.setOnClickListener { dogClicked(dogs[adapterPosition]) }
    }

    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {
        holder.loadDog(dogs[position])
    }

    override fun getItemCount(): Int = dogs.size

    class DogViewHolder(private val binding: ItemDogBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadDog(dogStr: String) = with(binding) {
            ivDog.load(dogStr)
        }
        companion object {
            fun getInstance(parent: ViewGroup) = ItemDogBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> DogViewHolder(binding) }
        }
    }
}





















